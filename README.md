# Sqrl

Secure Quick Reliable Login is an pubkey authentication protocol.

This implementation has the goal to be usable with the Android app provided by [kalaspuffar at Github](https://github.com/kalaspuffar/secure-quick-reliable-login).

The original (not very nice to read) SQRL specification can be found at [grc.com](https://www.grc.com/sqrl/protocol.htm).

## Installation

Sqrl is [available in Hex](https://hex.pm/packages/sqrl), the package can be installed
by adding `sqrl` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:sqrl, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and is published on [HexDocs](https://hexdocs.pm/sqrl). The docs can
be found at [https://hexdocs.pm/sqrl](https://hexdocs.pm/sqrl).

# Limitations

This project is not ready for production.

Only supports `sqrl://` links, because `qrl://` links would result in unencrypted communicatiom which I don't want to support.

# Related projects

* [Android client at Github](https://github.com/kalaspuffar/secure-quick-reliable-login)
  and on [Google Play](https://play.google.com/store/apps/details?id=org.ea.sqrl)

* [Old, unmaintained(?) Ruby on Rails implementation at Github](https://github.com/tokenshift/Squarrel)
