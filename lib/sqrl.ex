defmodule Sqrl do
  @moduledoc """
  SQRL server implementation
  """

  alias Plug.Conn
  require Ed25519

  alias Sqrl.Nut

  @expiration_time 3*60*1000

  defstruct [
    valid?: false,
    sqrl_id: nil,
    errors: [],
    flags: [],

    client: nil,
    server: nil,
    ids: nil,
    message: nil
  ]

  @doc """
  Generate a new link to start SQRL authentication.
  """
  def link(host, path, ip) when is_binary(ip) do
    nut = Nut.generate(ip)
    link host, path, nut
  end

  @doc """
  Create a sqrl link to display in a QR code.
  """
  def link host, path, nut = %Nut{} do
    nut = nut |> Nut.to_string()
    query = URI.encode_query %{nut: nut}

    %URI{
      query: query,
      path: path,
      host: host,
      scheme: "sqrl"
    }
    |> URI.to_string()
  end

  @doc """
  Parse the content of a POST request from a SQRL client
  """
  def parse(body) when is_binary(body) do
    parse Conn.Query.decode(body)
  end

  def parse %{"client" => client, "server" => server, "ids" => ids} do
    client_data = parse_client(client)
    server_data = parse_server(server)
    signature = parse_ids(ids)
    message = client <> server

    %Sqrl{server: server_data, client: client_data, ids: signature, message: message}
  end

  @doc """
  Parse and validate the content of a POST request
  """
  def validate body do
    parse(body)
    |> Map.put(:valid?, true)
    |> validate_signature()
    |> validate_time()
    |> set_sqrl_id()
  end

  defp validate_signature(%Sqrl{ids: signature, message: message, client: %{idk: key}} = data) do
    case Ed25519.valid_signature?(signature, message, key) do
      true -> data
      _ ->
        data
        |> validation_error("invalid signature")
        |> add_flag(:client_error)
    end
  end

  # add validation error
  defp validation_error %Sqrl{valid?: _, errors: errors} = data, error do
    %{data | valid?: false, errors: [error | errors]}
  end

  def add_flag(%Sqrl{flags: flags} = data, flag) when is_atom(flag) do
    %{data | flags: Enum.uniq([flag | flags])}
  end

  # check if the time inside the nut is valid
  defp validate_time %Sqrl{server: %{nut: %{timestamp: time}}} = data do
    cond do
      time > now() -> raise "invalid time: #{time} < #{now()}"
      time < (now() - @expiration_time) ->
        data
        |> validation_error("nut expired")
        |> add_flag(:cmd_error)
      true -> data
    end
  end

  # set error if time is not set
  defp validate_time(data = %Sqrl{}) do
    data
    |> validation_error("no nut/timestamp")
    |> add_flag(:client_error)
  end

  # Set sqrl_id if data is valid
  defp set_sqrl_id %Sqrl{valid?: true, client: %{idk: key}} = data do
    id = Base.url_encode64(key, padding: false)
    %Sqrl{data | sqrl_id: id}
  end

  # don't set for invalid data
  defp set_sqrl_id(%Sqrl{} = data), do: data

  @doc """
  Send a response to a request
  """
  def response_body %Sqrl{valid?: true, flags: flags, server: %{nut: nut, qry: path}} do
    """
    ver=#{1}\r
    nut=#{Nut.to_string(nut)}\r
    tif=#{create_tif(flags)}\r
    qry=#{path}\r
    """
    |> Base.url_encode64(padding: false)
  end

  @doc """
  Send a response to a request
  """
  def response_body %Sqrl{valid?: true, server: %{nut: nut, uri: uri}} do
    %{path: path} = URI.parse uri
    response_body(%{valid?: true, nut: nut, qry: path})
  end

  @doc """
  Send a question response to a request
  """
  def response_ask_body %Sqrl{valid?: true} = data, question, answers do
    content =
      response_body(data)
      |> Base.url_decode64!(padding: false)

    ask =
      [question | answers]
      |> Enum.map(fn x -> x |> Base.url_encode64(padding: false) end)
      |> Enum.join("~")

    content <> "ask=#{ask}\r\n"
    |> Base.url_encode64(padding: false)
  end


  defp create_tif flags do
    available = [
      id_match: 0x1, # client ID (ids) is known by server
      prev_id_match: 0x2, # previous client ID (pids) is known by server
      ip_match: 0x4, # IP of brwoser and phone match
      sqrl_diabled: 0x8, # SQRL is disabled for this client ID on the server
      unsupported_function: 0x10, # cmd from client is not supported
      transient_error: 0x20, # client should retry with new nut and qry
      cmd_error: 0x40, # comand failed on server (e.g. expired)
      client_error: 0x80, # command failed because of invalid client data
      bad_id: 0x100 # browser logged in with other client ID already
    ]
    flags
    |> Enum.map(fn flag -> available[flag] end)
    |> Enum.reduce(0, fn acc, x -> acc + x end)
    |> Integer.to_string(16)
  end


  defp parse_client client do
    {:ok, data} = Base.url_decode64 client, padding: false
    parsed =
      data
      |> String.split()
      |> Enum.map(fn x ->
        [k, v] = String.split(x, "=")
        {String.to_atom(k), v}
      end)
      |> Enum.into(%{})

    %{idk: idk} = parsed
    key = Base.url_decode64! idk, padding: false
    Map.merge parsed, %{ idk: key}
  end

  defp parse_server server do
    data = Base.url_decode64! server, padding: false
    cond do
      String.starts_with?(data, "sqrl://") -> parse_server_link(data)
      String.starts_with?(data, "ver=") -> parse_server_data(data)
      true -> raise "Unkonwn server data"
    end
  end

  defp parse_server_link uri do
    %{query: query, path: path} = URI.parse(uri)
    %{"nut" => nut_s} = URI.decode_query(query)
    nut = Nut.from_string(nut_s)
    %{nut: nut, uri: uri, qry: path, nut_s: nut_s}
  end

  defp parse_server_data data do
    parsed =
      data
      |> String.split()
      |> Enum.map(fn x ->
        [k, v] = String.split(x, "=")
        {String.to_atom(k), v}
      end)
      |> Enum.into(%{})
    %{nut: nut_s} = parsed
    nut = Nut.from_string(nut_s)
    Map.merge parsed, %{nut: nut, nut_s: nut_s}
  end

  defp parse_ids ids do
    Base.url_decode64! ids, padding: false
  end

  defp now do
    :os.system_time(:millisecond)
  end

end
