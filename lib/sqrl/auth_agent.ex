defmodule Sqrl.AuthAgent do
  @moduledoc """
  Storage of successfull authentication requests
  """
  use Agent

  def start_link(_) do
    Agent.start_link(fn -> Map.new end, name: __MODULE__)
  end

  @doc "Check if the nut authenticated"
  def authenticated?(nut) do
    Agent.get(__MODULE__, fn map ->
      case Map.get(map, nut) do
        nil -> {:error, "not authenticated"}
        data -> {:ok, data}
      end
    end)
  end

  @doc "Marks a nut as authenticated"
  def put_authenticated(nut, data) do
    Agent.update(__MODULE__, &Map.put(&1, nut, data))
  end

  @doc "Removes nut after login is complete"
  def login_complete nut do
    Agent.get_and_update(__MODULE__, fn map ->
      {Map.get(map, nut), Map.drop(map, [nut])}
    end)
  end

  @doc "Remove all open authentication"
  def remove_all() do
    Agent.get_and_update(__MODULE__, fn map ->
      {map, %{}}
    end)
  end
end
