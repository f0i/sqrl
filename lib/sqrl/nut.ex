defmodule Sqrl.Nut do
  @moduledoc """
  SQRL nonce
  """

  @key (ExCrypto.generate_aes_key(:aes_256, :bytes) |> case do
        {:ok, val} -> val
      end)

  @enforce_keys [:ip, :timestamp]

  defstruct [
    ip: nil,
    timestamp: nil,
  ]

  @doc """
  Generate a new `%Nut{}` with the IP from parameter and current timestamp in ms.
  """
  def generate(ip) when is_binary(ip) do
    timestamp = now()
    %Sqrl.Nut{ip: ip, timestamp: timestamp}
  end

  @doc """
  Decrypt a string and return the encoded `%Nut{}`
  """
  def from_string(nut) when is_binary(nut) do
    %{"ip" => ip, "ts" => timestamp} = decrypt(nut)
    %Sqrl.Nut{ip: ip, timestamp: timestamp}
  end

  @doc """
  Encrypt the IP and timestamp and encode into an URL safe string
  """
  def to_string(%Sqrl.Nut{ip: ip, timestamp: timestamp}) do
    %{ip: ip, ts: timestamp}
    |> encrypt()
  end

  defp now do
    :os.system_time(:millisecond)
  end

  defp encrypt message do
    plain_text = Poison.encode!(message)
    {:ok, {init_vec, cipher_text}} = ExCrypto.encrypt(@key, plain_text)
    Base.url_encode64(init_vec, padding: false) <> "." <> Base.url_encode64(cipher_text, padding: false)
  end

  defp decrypt encrypted do
    [init_vec, cipher_text] =
      encrypted
      |> String.split(".")
      |> Enum.map(fn x -> Base.url_decode64!(x, padding: false) end)

    ExCrypto.decrypt(@key, init_vec, cipher_text)
    |> ok!()
    |> Poison.decode!()
  end

  defp ok!({:ok, val}), do: val

end
