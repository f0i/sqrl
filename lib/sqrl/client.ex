defmodule Sqrl.Client do
  @moduledoc """
  Client implementation of SQRL
  """

  require Ed25519

  @doc """
  Create POST body for a query command
  """
  def query(link, priv, pub) do
    server = server_string link

    pubkey = pub |> Base.url_encode64(padding: false)

    client =
      client_string(%{
        ver: 1,
        cmd: "query",
        idk: pubkey,
        opt: "",
        btn: ""
        })

      signed_post_body(server, client, priv, pub)
  end

  @doc """
  Create POST body for a ident command
  """
  def ident(link, priv, pub) do
    server = server_string link

    pubkey = pub |> Base.url_encode64(padding: false)

    client =
      client_string(%{
        ver: 1,
        cmd: "ident",
        idk: pubkey,
        opt: "",
        btn: ""
        })

      signed_post_body(server, client, priv, pub)
  end

  # Sign and create POST body
  defp signed_post_body(server, client, priv, pub) when is_binary(pub) and is_binary(priv) do
    message = client <> server

    signature =
      message
      |> Ed25519.signature(priv, pub)
      |> Base.url_encode64(padding: false)

    "server=#{server}&client=#{client}&ids=#{signature}"
  end

  defp client_string(client = %{ver: 1, cmd: cmd, idk: pubkey}) do
    idk_s = "ver=1\r\ncmd=#{cmd}\r\nidk=#{pubkey}"
    opt_s =
      case Map.get client, :opt do
        nil -> idk_s
        arr = [_] -> idk_s <> "\r\nopt=#{Enum.join arr, "~"}"
        str -> idk_s <> "\r\nopt=#{str}"
      end
    btn_s =
      case Map.get client, :btn do
        nil -> opt_s
        int when is_integer(int)-> opt_s <> "\r\nbtn=#{Integer.to_string(int)}"
        str -> opt_s <> "\r\nbtn=#{str}"
      end

    _client_s = btn_s |> Base.url_encode64(padding: false)
  end

  defp server_string(server) when is_binary(server) do
    server |> Base.url_encode64(padding: false)
  end

  defp server_string(%{ver: 1, nut: nut, tif: tif, qry: qry} = server) do
    """
    ver=1\r
    nut=#{nut}\r
    tif=#{tif}\r
    qry=#{qry}\r
    """
    |> Base.url_encode64(padding: false)
  end
end
