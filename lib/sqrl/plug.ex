defmodule Sqrl.Plug do
  @moduledoc """
  Plug to handle SQRL requests
  """
  require Sqrl
  alias Plug.Conn

  def validate %Conn{method: "POST", body_params: data = %{"client" => _, "ids" => _, "server" => _}} do
    Sqrl.validate data
  end

  def validate conn = %Conn{method: "POST", body_params: %Plug.Conn.Unfetched{}} do
    {:ok, body, _conn} = Conn.read_body(conn)
    Sqrl.validate body
  end

  def respond conn = %Conn{}, %Sqrl{valid?: true} = data do
    body = Sqrl.response_body data
    Conn.send_resp conn, 200, body
  end

  def complete conn = %Conn{} do
    %{query_string: query} = conn
    %{"link" => link} = URI.decode_query(query)
    %{query: query} = URI.parse(link)
    %{"nut" => nut} = URI.decode_query(query)
    nut
  end

end
