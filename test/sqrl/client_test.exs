defmodule Sqrl.ClientTest do
  @moduledoc """
  Test Sqrl.Client
  """

  use ExUnit.Case
  require Sqrl.Client

  @priv Base.decode64!("1rwbU/lSl4X0dbhzHWUudS7YqlzTBq3s1zhLauHuQ1o=")
  @pub Base.decode64!("G+0J3YZPMweBF1VmLG2xXifqZ9fwPySNA/ScEw6elnY=")

  describe "Initial link:" do
    test "Generate query" do
      body = Sqrl.Client.query("sqrl://f0i.de/sqrl?nut=ABCDEF.ABCDEF", @priv, @pub)

      assert body ==
        "server=c3FybDovL2YwaS5kZS9zcXJsP251dD1BQkNERUYuQUJDREVG&client=dmVyPTENCmNtZD1xdWVyeQ0KaWRrPUctMEozWVpQTXdlQkYxVm1MRzJ4WGlmcVo5ZndQeVNOQV9TY0V3NmVsblkNCm9wdD0NCmJ0bj0&ids=8PHCBr9yw-kiqW0mQ7YZh4lUO835Mvgzi1tHY4w5q7_X5Yq2BEKuw_Q0AEr2dKMWerf3eisf3_bIOIjq0SD7CA"
    end

    test "Generate ident" do
      body = Sqrl.Client.ident("sqrl://f0i.de/sqrl?nut=ABCDEF.ABCDEF", @priv, @pub)

      assert body ==
        "server=c3FybDovL2YwaS5kZS9zcXJsP251dD1BQkNERUYuQUJDREVG&client=dmVyPTENCmNtZD1pZGVudA0KaWRrPUctMEozWVpQTXdlQkYxVm1MRzJ4WGlmcVo5ZndQeVNOQV9TY0V3NmVsblkNCm9wdD0NCmJ0bj0&ids=ve69t57QEVh4NENSrFusoHqu5CWe_Grf2syyTgj0y54BrfAbLlsKC_N2R807SW8rvsWuYL0dlfnPNwevqtG4Cw"
    end
  end

  describe "From response:" do
    test "Generate query" do
      nut = "ABCDEF.ABCDEF"
      tif = 1
      qry = "/auth/sqrl"
      body = Sqrl.Client.query(%{ver: 1, nut: nut, tif: tif, qry: qry}, @priv, @pub)

      assert body ==
        "server=dmVyPTENCm51dD1BQkNERUYuQUJDREVGDQp0aWY9MQ0KcXJ5PS9hdXRoL3NxcmwNCg&client=dmVyPTENCmNtZD1xdWVyeQ0KaWRrPUctMEozWVpQTXdlQkYxVm1MRzJ4WGlmcVo5ZndQeVNOQV9TY0V3NmVsblkNCm9wdD0NCmJ0bj0&ids=0haUPeRn1XspfpkO0SjXYVo2IDdWj5e3A5_UnIV5RbQgBX9ywsMGaAJklKWzHTiFZ4OVPUg2tvDId4AzTzatAg"
    end

    test "Generate ident" do
      nut = "ABCDEF.ABCDEF"
      tif = 1
      qry = "/auth/sqrl"
      body = Sqrl.Client.ident(%{ver: 1, nut: nut, tif: tif, qry: qry}, @priv, @pub)

      assert body ==
        "server=dmVyPTENCm51dD1BQkNERUYuQUJDREVGDQp0aWY9MQ0KcXJ5PS9hdXRoL3NxcmwNCg&client=dmVyPTENCmNtZD1pZGVudA0KaWRrPUctMEozWVpQTXdlQkYxVm1MRzJ4WGlmcVo5ZndQeVNOQV9TY0V3NmVsblkNCm9wdD0NCmJ0bj0&ids=Jk96hKLTAIXG2YNIxiQukNMqGSfu01QNexndZGzhreiSwaaVxQktyXqpuOAnU2oKMZWE101nsxL4-Kr5kZCnCg"
    end
  end

end
