defmodule Sqrl.PlugTest do
  use ExUnit.Case
  use Plug.Test

  alias Sqrl.Nut
  alias Plug.Conn

  doctest Sqrl.Plug

  @priv Base.decode64!("1rwbU/lSl4X0dbhzHWUudS7YqlzTBq3s1zhLauHuQ1o=")
  @pub Base.decode64!("G+0J3YZPMweBF1VmLG2xXifqZ9fwPySNA/ScEw6elnY=")

  test "parse and validate" do
    data =
      initial_post()
      |> Sqrl.Plug.validate()

    assert %Sqrl{valid?: true} = data
  end

  test "send response" do
    conn = initial_post()

    data =
      conn
      |> Sqrl.Plug.validate()

    %Plug.Conn{} =
      conn
      |> Sqrl.Plug.respond(data);
  end

  test "auth example" do
    # First request by client
    # -----------------------

    # start AuthAgent
    assert {:ok, pid} = Sqrl.AuthAgent.start_link(0)
    assert Process.alive?(pid)

    # parse data
    conn = initial_post()
    %{sqrl_id: id} = data = Sqrl.Plug.validate(conn)

    # find user
    _user = %{name: "Dummy", id: id}

    # respond to client
    Sqrl.Plug.respond(conn, data)


    # Second request by client
    # ------------------------

    # ident request
    conn = ident_post()
    %{valid?: true, server: %{nut_s: nut}, sqrl_id: id} = data = Sqrl.Plug.validate(conn)

    # set user login pending
    if id != nil do
      user = %{name: "Dummy", id: id}
      Sqrl.AuthAgent.put_authenticated(nut, user)
    end

    # respond to client
    Sqrl.Plug.respond(conn, data)

    # check if everything worked
    assert id != nil

    # Third request by browser
    # ------------------------
    conn = auth_get(nut)

    nut = Sqrl.Plug.complete(conn)
    user = Sqrl.AuthAgent.login_complete(nut)

    Conn.send_resp(conn, 200, "Logged in as #{inspect user}")

    # check if everything worked
    assert %{id: _, name: "Dummy"} = user
  end

  # query POST request
  defp initial_post do
    link = Sqrl.link("sqrl.f0i.de", "/auth/sqrl", "127.0.0.1")
    body = Sqrl.Client.query link, @priv, @pub
    conn(:post, "/auth/sqrl", body)
  end

  # ident POST request
  defp ident_post do
    nut = Nut.generate("127.0.0.1") |> Nut.to_string()
    tif = 1
    qry = "/auth/sqrl"
    body = Sqrl.Client.ident(%{ver: 1, nut: nut, tif: tif, qry: qry}, @priv, @pub)
    conn(:post, "/auth/sqrl", body)
  end

  # auth GET request
  defp auth_get nut do
    link = "sqrl://sqrl.f0i.de/auth/sqrl?nut=#{nut}"
    conn(:get, "/auth/sqrl?link=#{URI.encode(link)}")
  end
end
