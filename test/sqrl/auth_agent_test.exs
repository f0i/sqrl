defmodule Sqrl.AuthAgentTest do
  @moduledoc """
  Storage of successfull authentication requests tests
  """

  use ExUnit.Case

  test "Add authenticated client" do
    assert {:ok, pid} = Sqrl.AuthAgent.start_link(0)
    assert Process.alive?(pid)
  end

  test "check authenticated?" do
    assert {:ok, _pid} = Sqrl.AuthAgent.start_link(0)

    assert :ok = Sqrl.AuthAgent.put_authenticated("NUT", %{user: 123})
    assert {:ok, %{user: 123}} = Sqrl.AuthAgent.authenticated?("NUT")
    assert {:error, "not authenticated"} = Sqrl.AuthAgent.authenticated?("NUT2")
  end

  test "clear pending authentications" do
    assert {:ok, _pid} = Sqrl.AuthAgent.start_link(0)

    assert :ok = Sqrl.AuthAgent.put_authenticated("NUT", %{user: 123})
    assert :ok = Sqrl.AuthAgent.put_authenticated("NUT2", %{user: 456})
    assert {:ok, %{user: 123}} = Sqrl.AuthAgent.authenticated?("NUT")
    assert {:ok, %{user: 456}} = Sqrl.AuthAgent.authenticated?("NUT2")

    assert %{"NUT" => _} = Sqrl.AuthAgent.remove_all()
    assert {:error, "not authenticated"} = Sqrl.AuthAgent.authenticated?("NUT")
    assert {:error, "not authenticated"} = Sqrl.AuthAgent.authenticated?("NUT2")
  end

  test "complete login" do
    assert {:ok, _pid} = Sqrl.AuthAgent.start_link(0)

    assert :ok = Sqrl.AuthAgent.put_authenticated("NUT", %{user: 123})
    assert :ok = Sqrl.AuthAgent.put_authenticated("NUT2", %{user: 456})
    assert {:ok, %{user: 123}} = Sqrl.AuthAgent.authenticated?("NUT")
    assert {:ok, %{user: 456}} = Sqrl.AuthAgent.authenticated?("NUT2")

    assert %{user: 123} == Sqrl.AuthAgent.login_complete("NUT")

    assert {:error, "not authenticated"} = Sqrl.AuthAgent.authenticated?("NUT")
    assert {:ok, %{user: 456}} = Sqrl.AuthAgent.authenticated?("NUT2")
  end

end
