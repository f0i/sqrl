defmodule Sqrl.NutTest do
  use ExUnit.Case
  alias Sqrl.Nut
  doctest Nut

  describe "generating a nut" do
    test "should return an encrypted string" do
      nut =
        Nut.generate("127.0.0.1")
        |> Nut.to_string()

      assert nut =~ ~r/[0-9a-z_.-]{32,}/i
    end
  end

  describe "encrypt nut" do
    test "can be decrypted" do
      nut1 = Nut.generate("127.0.0.1")
      nut2 =
        nut1
        |> Nut.to_string()
        |> Nut.from_string()

      assert nut1 == nut2
    end
  end

end
