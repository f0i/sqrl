defmodule SqrlTest do
  use ExUnit.Case

  require Ed25519
  alias Sqrl.Nut

  doctest Sqrl

  @priv Base.decode64!("1rwbU/lSl4X0dbhzHWUudS7YqlzTBq3s1zhLauHuQ1o=")
  @pub Base.decode64!("G+0J3YZPMweBF1VmLG2xXifqZ9fwPySNA/ScEw6elnY=")

  test "create link" do
    link = Sqrl.link("sqrl.f0i.de", "/auth/sqrl", "127.0.0.1")
    assert link =~ ~r{^sqrl://sqrl.f0i.de/auth/sqrl\?nut=[^=& ]+$}
  end

  describe "POST response" do
    test "can parse" do
      data =
        initial_post()
        |> Sqrl.parse

      assert %{server: %{nut: %Nut{}}, client: %{ver: "1", cmd: _, idk: _}, ids: _} = data
    end

    test "can parse subsequent post requests" do
      data =
        subsequent_post()
        |> Sqrl.parse

      assert %Sqrl{server: %{nut: %Nut{}}, client: %{ver: "1", cmd: _}} = data
      assert %Sqrl{valid?: false} = data
    end

    test "can validate" do
      data =
        initial_post()
        |> Sqrl.validate

      assert %{client: %{ver: "1"}, valid?: true, sqrl_id: id} = data
      assert id != nil
    end

    test "detects expired nut" do
      data =
        expired_post()
        |> Sqrl.validate

      assert %{client: %{ver: "1"}, valid?: false, flags: flags} = data
      assert flags |> Enum.member?(:cmd_error)
    end

    test "detects invalid signature" do
      data =
        invalid_signature_post()
        |> Sqrl.validate

      assert %{client: %{ver: "1"}, valid?: false, flags: flags} = data
      assert flags |> Enum.member?(:client_error)

    end
  end

  describe "handle post" do
    test "reply to query with nut in link" do
      body =
        initial_post()
        |> Sqrl.validate()
        |> Sqrl.response_body()

      assert {:ok, data} = Base.url_decode64(body, padding: false)
      assert data =~ ~r/ver=1\r?\n/
      assert data =~ ~r/nut=[^=]*\r?\n/
      assert data =~ ~r{qry=/auth/sqrl\r?\n}
      assert data =~ ~r/tif=[0-9]+\r?\n/
    end

    test "reply to query with nut in server data" do
      body =
        subsequent_post()
        |> Sqrl.validate()
        |> Sqrl.response_body()

      assert {:ok, data} = Base.url_decode64(body, padding: false)
      assert data =~ ~r/ver=1\r?\n/
      assert data =~ ~r/nut=[^=]+\r?\n/
      assert data =~ ~r{qry=/auth/sqrl\r?\n}
      assert data =~ ~r/tif=[0-9]+\r?\n/
    end

    test "reply with question" do
      body =
        initial_post()
        |> Sqrl.validate()
        |> Sqrl.response_ask_body("Question", ["Answer 1", "Answer 2"])

      assert {:ok, data} = Base.url_decode64(body, padding: false)
      assert data =~ ~r/ver=1\r?\n/
      assert data =~ ~r/nut=[^=]*\r?\n/
      assert data =~ ~r{qry=/auth/sqrl\r?\n}
      assert data =~ ~r/tif=[0-9]+\r?\n/
      assert data =~ ~r/ask=[^=]+\r?\n/
    end
  end

  describe "test transaction information flags" do
    test "no flags" do
      body =
        subsequent_post()
        |> Sqrl.validate()
        |> Map.put(:flags, [])
        |> Sqrl.response_body()

      assert {:ok, data} = Base.url_decode64(body, padding: false)
      assert data =~ ~r/tif=0\r?\n/
    end

    test "ID match" do
      body =
        subsequent_post()
        |> Sqrl.validate()
        |> Map.put(:flags, [:id_match])
        |> Sqrl.response_body()

      assert {:ok, data} = Base.url_decode64(body, padding: false)
      assert data =~ ~r/tif=1\r?\n/
    end

    test "IP match" do
      body =
        subsequent_post()
        |> Sqrl.validate()
        |> Map.put(:flags, [:ip_match])
        |> Sqrl.response_body()

      assert {:ok, data} = Base.url_decode64(body, padding: false)
      assert data =~ ~r/tif=4\r?\n/
    end

    test "IP and ID match" do
      body =
        subsequent_post()
        |> Sqrl.validate()
        |> Map.put(:flags, [:id_match, :ip_match])
        |> Sqrl.response_body()

      assert {:ok, data} = Base.url_decode64(body, padding: false)
      assert data =~ ~r/tif=5\r?\n/
    end

    test "previous ID match and cmd error" do
      body =
        subsequent_post()
        |> Sqrl.validate()
        |> Map.put(:flags, [:prev_id_match, :cmd_error])
        |> Sqrl.response_body()

      assert {:ok, data} = Base.url_decode64(body, padding: false)
      assert data =~ ~r/tif=42\r?\n/
    end

  end

  describe "flag setter" do
    test "can set flags" do
      data =
        subsequent_post()
        |> Sqrl.validate()
        |> Sqrl.add_flag(:id_match)
        
      assert %Sqrl{flags: [:id_match]} = data
    end
  end

  defp initial_post do
    link = Sqrl.link("sqrl.f0i.de", "/auth/sqrl", "127.0.0.1")
    _body = Sqrl.Client.query link, @priv, @pub
    #conn(:post, "/auth/sqrl", body)
  end

  defp subsequent_post do
    nut = Nut.generate("127.0.0.1") |> Nut.to_string()
    tif = 1
    qry = "/auth/sqrl"
    _body = Sqrl.Client.query(%{ver: 1, nut: nut, tif: tif, qry: qry}, @priv, @pub)
  end

  defp expired_post do
    link = Sqrl.link("sqrl.f0i.de", "/auth/sqrl", %Nut{timestamp: 1234567890123, ip: "127.0.0.1"})
    _body = Sqrl.Client.query link, @priv, @pub
  end

  defp invalid_signature_post do
    nut = %Nut{timestamp: 1234567890123, ip: "127.0.0.1"}
    link = Sqrl.link("sqrl.f0i.de", "/auth/sqrl", nut)

    _body =
      Sqrl.Client.query(link, @priv, @pub)
      |> String.replace(~r/ids=.{43}/, "ids=WLSAMFqok9k2BrWXQZIm3brA38uO0X3Hw1ZJfE60PuE")
  end

end
