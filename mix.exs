defmodule Sqrl.MixProject do
  use Mix.Project

  def project do
    [
      app: :sqrl,
      version: "0.3.0",
      description: "SQRL authentication",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:poison, "~> 3.1"},
      {:ex_crypto, "~> 0.9.0"},
      {:ed25519, "~> 1.3"},
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:mix_test_watch, "~> 0.6", only: :dev, runtime: false},
      {:plug, "~> 1.6"},
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end

  defp package do
    [files: ~w(lib mix.exs README.md),
     maintainers: ["Martin Sigloch"],
     licenses: ["GPLv3"],
     links: %{"GitLab" => "https://gitlab.com/f0i/sqrl"}]
  end
end
