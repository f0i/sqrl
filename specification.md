

# SQRL protocol

3 Parties:

Server: Webserver you want to login to
Browser: Client which shold get logged in
Phone: SQRL authentication device or app

Steps:

* Server generates URI and send to Browser
* Browser shows URI as an QR-Code to Phone
* Phone signs the URI and sends it to the Server with its public key
* Server marks Browser as authenticated


## Generate URI

The generated URI has the following format:

    sqrl://<host>/<path>?nut=<encrypted data>

The `sqrl://` will be replaced with `https://` and used to send back data to the Server later.

The encrypted data can be used to store informatin for authentication so the Server doesn't have to keep it in memory.

In this implementation it contains the Browser IP and the current timestamp to expire the URI after some minutes.


### Example

    sqrl://sqrl.f0i.de/auth/sqrl?nut=Kb3B0PW5vaXB0ZXN0DQppZGs9cFgtMUFCUXZmRTZhY0


## Display URI

The URI can be displaied as an QR-Code or any other way the Phone can read it.


## Signing the URI

The phone signs the URI and could pass additional options.


## Send signed data to Server

POST request will be send to the following link (see Generate URI section):

    https://<host>/<path>?nut=<encrypted data>

The content of the POST request is as follows:

    client=<client data>&server=<server data>&ids=<id data>

client data:

```
ver=1
cmd=query
opt=noiptest
idk=<base64url(pubkey)>
```

server data:
`base64url(<URI>)`

ids:
`base64url(signature)`


### Example

```
POST /sqrl?nut=BXQC3ONGqtf5LU4jlqg0Tw HTTP/1.0
Connection: close
Content-Length: 279
Content-Type: application/x-www-form-urlencoded
User-Agent: Dalvik/2.1.0 (Linux; U; Android 8.0.0; xxxxxx)
Accept-Encoding: gzip

client=dmVyPTENCmNtZD1xdWVyeQ0Kb3B0PW5vaXB0ZXN0DQppZGs9cFgtMUFCUXZmRTZhY0k0SjVwUzAzLWhwSDNBbjI3M1RIb01aejBSLXZoaw&server=c3FybDovL3Rlc3QuZjBpLmRlL3Nxcmw_bnV0PUJYUUMzT05HcXRmNUxVNGpscWcwVHc&ids=C4Hu9L2e4VcZNmoCFiKMCW5GjiTmE-21ZzvtuRv-yPWCcOJqSQAmNQeAI8RlYc9z2VwHsRYoZ5F27UiAn64hAQ
```

client data:

```
base64url(
ver=1
cmd=query
opt=noiptest
idk=pX-1ABQvfE6acI4J5pS03-hpH3An273THoMZz0R-vhk
)
```

idk:
`base64url(pubkey)`

server data:
`base64url(sqrl://test.f0i.de/sqrl?nut=BXQC3ONGqtf5LU4jlqg0Tw)`

ids:
`base64url(signature)`


